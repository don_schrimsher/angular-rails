// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
//= require jquery
//= require angular/angular
//= require angular-ui-router/release/angular-ui-router
//= require angular-resource/angular-resource
//= require angular-rails-templates
//= require angular-flash/dist/angular-flash
//= require app/app.module
//= require app/app.routes
//= require_tree .