angular.module('home', []);
angular.module('angularRails', [
	'ui.router',
	'ngResource',
	'templates',
	'home',
	'angular-flash.service',
	'angular-flash.flash-alert-directive'
	]);