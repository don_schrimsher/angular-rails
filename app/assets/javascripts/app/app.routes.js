var app = angular.module('angularRails')
app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('home', {
		url: '/', 
		templateUrl: 'app/components/home/home.html',
		controller: 'HomeController as home'
	});

	$urlRouterProvider.otherwise('/')
}]);