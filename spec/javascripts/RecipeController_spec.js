describe('RecipesController', function() {
	var scope = null,
	ctrl = null,
	location = null,
	routeParams = null,
	resource = null,
	httpBackend = null,
	flash = null,
	recipeId = 42,
	fakeRecipe = {
		id: recipeId,
		name: "Baked Potatoes",
		instructions: "Pierce with fork, nuke for 20 minutes."
	};

	function setUpController(recipeExists) {
		inject(function($location, $routeParams, $rootScope, $httpBackend, $controller, _flash_) {
			var request = new RegExp("\/recipes/" + recipeId),
				results;
			if(recipeExists) {
				results = [200, fakeRecipe];
			} else {
				results = [404];
			}
			scope = $rootScope.$new();
			location = $location;
			routeParams = $routeParams;
			routeParams.recipeId = recipeId;
			httpBackend = $httpBackend;
			httpBackend.expectGET(request).respond(results[0],results[1])
			flash = _flash_;

			ctrl = $controller('RecipeController', {
				$scope: scope
			});

		});
	}

	beforeEach(module("receta"));

	afterEach(function() {
		httpBackend.verifyNoOutstandingExpectation();
		httpBackend.verifyNoOutstandingRequest();
	});

	describe('controller initialization', function() {
		describe('recipe is found', function() {
			beforeEach(function() {
				setUpController(true);
			});

			it('loads the given recipe', function() {
				httpBackend.flush();
				expect(scope.recipe).toEqualData(fakeRecipe);
			});
		});

		describe('recipe is not found', function() {
			beforeEach(function() {
				setUpController(false);
			});

			it('loads the given recipe', function() {
				httpBackend.flush();
				expect(scope.recipe).toBe(null);
				expect(flash.error).toBe("There is no recipe with ID " + recipeId);
			});
		});
	});
});