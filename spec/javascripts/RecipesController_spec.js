describe('RecipesController', function() {
	var scope = null,
	ctrl = null,
	location = null,
	routeParams = null,
	resource = null,
	httpBackend = null;

	function setUpController(keywords, results) {
		inject(function($location, $routeParams, $rootScope, $resource, $httpBackend, $controller) {
			scope = $rootScope.$new();
			location = $location;
			resource = $resource;
			routeParams = $routeParams;
			routeParams.keywords = keywords;
			httpBackend = $httpBackend;

			ctrl = $controller('RecipesController', {
				$scope: scope,
				$location: location
			});
		});

		if(results) {
			var request = new RegExp("\/recipes.*keywords=" + keywords)
			httpBackend.expectGET(request).respond(results);
		}
	}

	beforeEach(module("receta"));

	afterEach(function() {
		httpBackend.verifyNoOutstandingExpectation();
		httpBackend.verifyNoOutstandingRequest();
	});

	describe('controller initialization', function() {
		describe('when no keywords present', function() {
			beforeEach(function() {
				setUpController();
			});

			it('defaults to no recipes', function() {
				expect(scope.recipes).toEqualData([]);
			});
		});

		describe('with keywords', function() {
			var keywords = 'foo',
			recipes = [{
				id: 2,
				name: 'Baked Potatoes'
			},
			{
				id: 4,
				name: 'Potatoes Au Gratin'
			}];
			beforeEach(function() {
				setUpController(keywords, recipes);
				httpBackend.flush();
			});

			it('calls the backend', function() {
				expect(scope.recipes).toEqualData(recipes);
			});
		});
	});

	describe('search', function() {
		beforeEach(function() {
			setUpController();
		});

		it('redirects to itself with a keyword param', function() {
			var keywords = 'foo';
			scope.search(keywords);
			expect(location.path()).toBe('/');
			expect(location.search()).toEqualData({keywords: keywords});
		});
	});
});